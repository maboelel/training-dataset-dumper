from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class FlowSelector(BaseBlock):
    """Matches a jet collection to a b-tagging collection.

    Parameters
    ----------
    jet_collection : str
        The name of the jet collection to match. If not provided, it will be taken from the dumper config.
    """

    jet_collection: str = None
    name: str = "NeutralFlowSelectorAlg"

    def __post_init__(self):
        if self.jet_collection is None:
            self.jet_collection = self.dumper_config["jet_collection"]
        

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addEventAlgo(
            CompFactory.FlowSelectorAlg(
                name=self.name,
                Constituents=f'{self.jet_collection}.constituentLinks',
                OutConstituentsNeutral=f'{self.jet_collection}.neutralConstituentLinks',
                OutConstituentsCharged=f'{self.jet_collection}.chargedConstituentLinks'
            )
        )
        
        return ca