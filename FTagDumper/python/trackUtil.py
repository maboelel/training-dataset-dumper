from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from Campaigns.Utils import Campaign

VARIATIONS = {
    'TRK_RES_D0_MEAS',
    'TRK_RES_Z0_MEAS',
    'TRK_RES_D0_MEAS_UP',
    'TRK_RES_Z0_MEAS_UP',
    'TRK_RES_D0_MEAS_DOWN',
    'TRK_RES_Z0_MEAS_DOWN',
    'TRK_RES_D0_DEAD',
    'TRK_RES_Z0_DEAD',
    'TRK_BIAS_D0_WM',
    'TRK_BIAS_Z0_WM',
    'TRK_BIAS_QOVERP_SAGITTA_WM',
    'TRK_FAKE_RATE_LOOSE',
    'TRK_FAKE_RATE_TIGHT',
    'TRK_EFF_LOOSE_GLOBAL',
    'TRK_EFF_LOOSE_IBL',
    'TRK_EFF_LOOSE_PP0',
    'TRK_EFF_LOOSE_PHYSMODEL',
    'TRK_EFF_TIGHT_GLOBAL',
    'TRK_EFF_TIGHT_IBL',
    'TRK_EFF_TIGHT_PP0',
    'TRK_EFF_TIGHT_PHYSMODEL',
    'TRK_EFF_LARGED0_GLOBAL',
    'TRK_EFF_LARGED0_IBL',
    'TRK_EFF_LARGED0_PP0',
    'TRK_EFF_LARGED0_PHYSMODEL',
    'TRK_EFF_LOOSE_TIDE',
    'TRK_FAKE_RATE_TIGHT_TIDE',
    'TRK_FAKE_RATE_LOOSE_TIDE',
    'TRK_FAKE_RATE_LOOSE_ROBUST',
}

###############################################################
# Function to merge the standard and lrt tracks
###############################################################
def LRTMerger(flags, name="InDetLRTMerge", **kwargs):
    acc = ComponentAccumulator()
    alg = CompFactory.CP.TrackParticleMergerAlg(name, **kwargs)
    kwargs.setdefault("TrackParticleLocation",
                      ["InDetTrackParticles", "InDetLargeD0TrackParticles"])
    kwargs.setdefault("OutputTrackParticleLocation",
                      "InDetWithLRTTrackParticles")
    kwargs.setdefault("CreateViewColllection", True)
    acc.addEventAlgo(alg, primary=True)
    return acc

def LRTGhostMerger(JetCollection="AntiKt10EMTopoRCJets"):
    ca = ComponentAccumulator()
    # combine the two ghost track collections into one
    jetGhostMergingAlg = CompFactory.CP.JetGhostMergingAlg("JetGhostMerger")
    jetGhostMergingAlg.JetCollection = JetCollection
    jetGhostMergingAlg.MergedGhostName = JetCollection + ".GhostTrackLRTMerged"
    jetGhostMergingAlg.InputGhostTrackNames = ["GhostTrack", "GhostTrackLRT"]
    ca.addEventAlgo(jetGhostMergingAlg)
    return ca

###############################################################
# Function to apply tracking  systematics
###############################################################
def applyTrackSys(flags, sys_list, input_track, jet, output_tracks=None):

    if output_tracks is None:
        output_tracks = "InDetTrackParticles_Sys"

    ca = ComponentAccumulator()
    # Please use prime numbers as the seeds to ensure higher scientific qualities
    TrackSmearingTool = CompFactory.InDet.InDetTrackSmearingTool()
    TrackSmearingTool.Seed = 2


    JetTrackFilterTool = CompFactory.InDet.JetTrackFilterTool()
    JetTrackFilterTool.Seed = 3

    TrackTruthFilterTool = CompFactory.InDet.InDetTrackTruthFilterTool()
    TrackTruthFilterTool.Seed = 5

    # You can provide a list of systematics and in the end it will
    # give us one container with all systematic uncertainties applied
    trackSysAlg = CompFactory.TrackSystematicsAlg(
        '_'.join(["TrackSysAlg", input_track, jet] + sys_list))
    trackSysAlg.systematic_variations = sys_list
    trackSysAlg.track_collection = input_track
    trackSysAlg.jet_collection = jet
    trackSysAlg.track_smearing_tool = TrackSmearingTool
    trackSysAlg.track_truth_filter_tool = TrackTruthFilterTool
    trackSysAlg.jet_track_filter_tool = JetTrackFilterTool
    trackSysAlg.output_track_collection = output_tracks
    # track biasing isn't supported outside MC20 campaigns at the
    # moment
    if flags.Input.MCCampaign in {Campaign[f'MC20{x}'] for x in 'ade'}:
        TrackBiasingTool = CompFactory.InDet.InDetTrackBiasingTool()
        trackSysAlg.track_biasing_tool = TrackBiasingTool
    ca.addEventAlgo(trackSysAlg)
    return ca
