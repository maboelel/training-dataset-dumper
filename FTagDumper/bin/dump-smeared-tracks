#!/usr/bin/env python3

"""

Dumps datasets with smeared tracks using Trigger/EFTracking/EFTrackingEmulation
for trigger upgrade studies

"""

import sys

from AthenaConfiguration.AllConfigFlags import initConfigFlags
from AthenaConfiguration.ComponentFactory import CompFactory
from EFTrackingEmulation.EFTrackingSmearingConfig import EFTrackingSmearingCfg
from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleAssociationAlgCfg

from FTagDumper import dumper

def getSmearConfig(combined_cfg):
    # Split dupmer and smear steering options
    if "dumper" in combined_cfg:
        dumper_cfg = combined_cfg.pop("dumper")
    else:
        raise ValueError(
            'Trying to load smearing configuration without '
            'the "dumper" group!')

    # Get smear configs
    smear_cfg = combined_cfg.pop("smear")

    if len(combined_cfg) > 0:
        raise ValueError(
            'Defined some unused options: ', combined_cfg.keys())
    
    return smear_cfg, dumper_cfg
    

def run():

    args = dumper.base_parser(__doc__).parse_args()

    flags = initConfigFlags()
    flags = dumper.update_flags(args, flags)
    flags.lock()

    ca = dumper.getMainConfig(flags)

    combined_cfg = dumper.combinedConfig(args.config_file)
    smear_cfg, dumper_cfg = getSmearConfig(combined_cfg)
    
    # Default container names
    jet_name = "AntiKt4EMTopoJets"
    input_track = "InDetTrackParticles"
    vertices = "PrimaryVertices"

    for cfg in smear_cfg:
        sf = cfg['smear_factor']
        sf_str = str(sf).replace(".","p")

        # Create smeared tracks block
        ca.merge(
            EFTrackingSmearingCfg(
                flags,
                f"EFTrackSmearing_SF{sf_str}",
                OutputTracksPtCutGeV=cfg["trk_pt"],
                SmearingScaleFactor=sf,
                InputTrackParticleContainer=input_track,
                SmearTruthParticle=False,
                SmearedTrackEfficiency=cfg["trk_eff"],
                ParameterizedTrackEfficiency=False,
                EnableMonitoring=True,
                OutputLevel=flags.Exec.OutputLevel))
        
        track_name = f"{input_track}_smeared_SF{sf_str}"

        # Augment the tracks
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
                f"PoorMansIpAugmenterAlg_SF{sf_str}",
                trackContainer=track_name,
                primaryVertexContainer=vertices))
    
        # Associate them to jets
        ca.merge(
            JetParticleAssociationAlgCfg(
                flags,
                JetCollection=jet_name,
                InputParticleCollection=track_name,
                OutputParticleDecoration=cfg["jet_track_decor"]))
    
    ca.merge(dumper.getBlocksConfig(flags, args))

    # Get truth jet z position
    ca.addEventAlgo(
        CompFactory.TruthJetPrimaryVertexDecoratorAlg(
            name="TruthJetPrimaryVertexDecorator",
            jetPvDecorator="AntiKt4TruthJets.PVz",
            useSignalProcessVertex=False, 
        ))
    ca.addEventAlgo(
        CompFactory.JetMatcherAlg(
            name=f'Matcher{jet_name}PVz',
            targetJet=jet_name,
            sourceJets=['InTimeAntiKt4TruthJets', 'AntiKt4TruthJets'],
            floatsToCopy={'PVz':'TruthJetPVz'},
            dR='deltaRToTruthJet',
            dPt='deltaPtToTruthJet',
            ptPriorityWithDeltaR=0.3,
            sourceMinimumPt=10000.,
        ))

    # debug printout
    ca.printConfig(withDetails=True, summariseProps=True)    
    ca.merge(dumper.getDumperConfig(args, config_dict=dumper_cfg))
    return ca.run()

if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
