#!/usr/bin/env python

"""
Dump some ftag trigger info!

This runs quite a few algorithms upstream of the dumping code, to
extract trigger tau jets from the trigger decisions, truth label them, and
compare them to offline reconstructed jets.
"""

import sys

from GaudiKernel.Constants import INFO

from FTagDumper import dumper
from FTagDumper import mctc
from FTagDumper import trigger as trig



def get_args():
    default_chain = 'HLT_tau20_perf_tracktwoMVA_L1eTAU12'
    dh = dict(help='(default: %(default)s)')
    parser = dumper.base_parser(__doc__)
    parser.add_argument('-t','--threads', type=int, default=0)
    parser.add_argument('-n','--chain', default=default_chain, **dh)
    return parser.parse_args()


def run():
    args = get_args()


    flags = dumper.update_flags(args)
    flags.Concurrency.NumThreads = args.threads
    if args.threads:
        flags.Concurrency.NumConcurrentEvents = args.threads

    flags.lock()

    combined_cfg = dumper.combinedConfig(args.config_file)
    trig_cfg, dumper_cfg = trig.getJobConfig(combined_cfg)

    #########################################################################
    ################### Build the component accumulator #####################
    #########################################################################
    #
    ca = dumper.getMainConfig(flags)

    if not trig_cfg["data"]:
        ca.merge(mctc.getMCTC())

    ca.merge(trig.tauDumper(
        flags,
        chain=args.chain,
        **trig_cfg
    ))
    ca.merge(dumper.getDumperConfig(args, config_dict=dumper_cfg))

    # Set Alg sequence to seqAND, no other algorithms shall be merged after
    dumper.setMainAlgSeqToSeqAND(ca)
    # print the configuration
    if flags.Exec.OutputLevel <= INFO:
        ca.printConfig(withDetails=True)

    #########################################################################
    ########################### Run everything ##############################
    #########################################################################
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
