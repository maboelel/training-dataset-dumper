#include "VariablesByType.hh"
#include "ConfigFileTools.hh"

void from_json(const nlohmann::ordered_json& node, VariablesByType& out) {
  namespace cft = ConfigFileTools;
  cft::OptionalConfigurationObject cfg(node);
#define ADD(NAME) out.NAME = cfg.get<VariablesByType::list_t>(#NAME, {})
  ADD(uchars);
  ADD(chars);
  ADD(ints);
  ADD(uints);
  ADD(halves);
  ADD(floats);
  ADD(doubles);
  ADD(ints_as_halves);
#undef ADD
  cfg.throw_if_unused("variables");
}

void from_json(const nlohmann::ordered_json& node,
               VariablesByCompression& out)
{
  namespace cft = ConfigFileTools;
  cft::OptionalConfigurationObject cfg(node);
#define ADD(NAME) out.NAME = cfg.get<VariablesByType::list_t>(#NAME, {})
  ADD(customs);
  ADD(compressed);
#undef ADD
  cfg.throw_if_unused("variables");
}
