/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRIGGER_TAUJET_GETTER_ALG_H
#define TRIGGER_TAUJET_GETTER_ALG_H

#include <AnaAlgorithm/AnaAlgorithm.h>

#include "TrigDecisionTool/TrigDecisionTool.h"
#include "xAODJet/JetContainer.h"
#include "xAODBTagging/BTaggingContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "JetInterface/IJetModifier.h"
#include "xAODEventInfo/EventInfo.h"

namespace Trig {
  class TrigDecisionTool;
}

class TriggerTauJetGetterAlg: public EL::AnaAlgorithm
{
public:
  TriggerTauJetGetterAlg(const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
private:
  PublicToolHandle<Trig::TrigDecisionTool> m_triggerDecision {
    this, "triggerDecisionTool", "",
      "Input TrigDecision Tool" };
  ToolHandleArray<IJetModifier> m_jetModifiers {
    this, "jetModifiers", {},
      "List of Jet modifiers" };
  Gaudi::Property<std::string> m_tauChain {
    this, "tauChain", "HLT_.*", "Regex for chains to match"};
  Gaudi::Property<std::vector<std::string>> m_additionalChains {
    this, "additionalChains", {}, "Additional required chains"};
  Gaudi::Property<bool> m_doTrackMatching {
    this, "doTrackMatching", true, "Switch to copy tracks from TauJet"};
  SG::ReadHandleKey< xAOD::TauJetContainer > m_trigTauJetKey  {
    this, "TrigTauJetCollectionKey", "HLT_taus",
    "Input TauJet collection name" };
  SG::WriteHandleKey<xAOD::JetContainer> m_outputJets {
    this, "outputJets", "SelectedJets", "Output jet container name"};
  SG::WriteDecorHandleKey<xAOD::JetContainer> m_trackLinkKey{
    this, "outputTrackAux", "TauTracks", "Output track aux name"};
};

#endif
