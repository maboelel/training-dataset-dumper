#include "DisplacedJetDecorator.hh"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"


// the constructor just builds the decorator
DisplacedJetDecorator::DisplacedJetDecorator(const std::string& link_name, const std::vector<int>& pdgIdList)
    : m_truth_particle_links(link_name),
      m_deco_is_displaced("isDisplaced"),
      m_deco_disp_pt_frac("displacedPtFraction"),
      m_deco_parent_llp_massGeV("parentLLPMassGeV"),
      m_deco_parent_llp_lifetime("parentLLPLifetime"),
      m_pdgIdList(pdgIdList) {}


std::tuple<float, float, float> DisplacedJetDecorator::getJetVar(const PartLinks& part_links) const {
    
 float disp_pt = 0.0;
  float prompt_pt = 0.0;
  float total_pt = 0.0;
  float parent_massGeV = NAN;
  float parent_lifetime = NAN;
  for (const PartLink& tl : part_links) {
    // cast to truth particle
    const auto* truth_particle = dynamic_cast<const xAOD::TruthParticle*>(*tl);

    if (!truth_particle) {
      throw std::runtime_error("Can't cast xAOD::IParticle to xAOD::TruthParticle");
    }

    // check for parent LLP
    const xAOD::TruthParticle* parent = checkProduction(*truth_particle);

    if(parent) {
        disp_pt += truth_particle->pt();
        parent_massGeV = parent->m()/1000;

        if (parent->hasProdVtx() && parent->hasDecayVtx()) {
	  parent_lifetime = (parent->decayVtx()->v4()-parent->prodVtx()->v4()).Vect().Mag();
        }
    } else {
        prompt_pt += truth_particle->pt();
    }
    total_pt += truth_particle->pt();
  }
  return std::tuple(disp_pt, parent_massGeV, parent_lifetime);

}

// check if truth particle originated from decay of particle in the pdgIdList, return pointer
// to particle if so
const xAOD::TruthParticle* DisplacedJetDecorator::checkProduction( const xAOD::TruthParticle & truthPart ) const {

  if (truthPart.nParents() == 0){
    return nullptr;
  } 
  else{
    const xAOD::TruthParticle * parent = truthPart.parent(0);
    if(not parent) {
      return nullptr;
    }
    if(std::find(m_pdgIdList.begin(), m_pdgIdList.end(), std::abs(parent->pdgId())) != m_pdgIdList.end()) {
        // const xAOD::TruthVertex* vertex = parent->decayVtx();
        // return parent->m()/1000; //GeV
        return parent;
    }
    // recurse on parent
    return checkProduction(*parent);
  }
  return nullptr;
}

// this call actually does the work on the jet
void DisplacedJetDecorator::decorate(
    const xAOD::Jet& jet) const {

  // get linked truth particles
  auto part_links = m_truth_particle_links(jet);

  // decorate
  auto [displacedPt, parentLLPMassGeV, parentLLPLifetime] = getJetVar(part_links);
  float pt_frac = displacedPt/jet.pt();

  m_deco_is_displaced(jet) = (pt_frac > 0.0);// apply selection > 0.35 in umami-preprocessing
  m_deco_disp_pt_frac(jet) = pt_frac;
  m_deco_parent_llp_massGeV(jet) = parentLLPMassGeV;         // = NAN if no parent LLP
  m_deco_parent_llp_lifetime(jet) = parentLLPLifetime; // "" 

}
