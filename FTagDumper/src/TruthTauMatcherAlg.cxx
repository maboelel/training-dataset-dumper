/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "TruthTauMatcherAlg.h"
#include "AsgMessaging/MessageCheck.h"

#include <memory>

TruthTauMatcherAlg::TruthTauMatcherAlg(const std::string& name, ISvcLocator* pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator)
{
  declareProperty("floatsToCopy", m_floats.toCopy);
  declareProperty("intsToCopy", m_ints.toCopy);
  declareProperty("uintsToCopy", m_uints.toCopy);
  declareProperty("charsToCopy", m_chars.toCopy);
  declareProperty("iparticlesToCopy", m_iparticles.toCopy);
}

// Load the Aux variables for the truth taus 
namespace {
  static const SG::AuxElement::ConstAccessor<char> acc_IsHadronicTau("IsHadronicTau");
  static const SG::AuxElement::ConstAccessor<double> acc_pt_vis("pt_vis");
  static const SG::AuxElement::ConstAccessor<double> acc_eta_vis("eta_vis");
  static const SG::AuxElement::ConstAccessor<double> acc_phi_vis("phi_vis");
  static const SG::AuxElement::ConstAccessor<double> acc_m_vis("m_vis");
  static const SG::AuxElement::ConstAccessor<unsigned int> acc_classifierParticleType("classifierParticleType");
}

StatusCode TruthTauMatcherAlg::initialize() {
  std::vector<std::string> truthTausCollection;
  for (const auto& key : m_truthTaus) {
    truthTausCollection.emplace_back(key.key());
  }
  std::string recoJetsCollection = m_recoJets.key();
  // std::string truthTausCollection = m_truthTaus.key();
  ATH_CHECK(m_floats.initialize(this, truthTausCollection, recoJetsCollection));
  ATH_CHECK(m_ints.initialize(this, truthTausCollection, recoJetsCollection));
  ATH_CHECK(m_uints.initialize(this, truthTausCollection, recoJetsCollection));
  ATH_CHECK(m_chars.initialize(this, truthTausCollection, recoJetsCollection));
  ATH_CHECK(m_iparticles.initialize(this, truthTausCollection, recoJetsCollection));

  m_drDecorator = recoJetsCollection + "." + m_dRKey;
  m_dPtDecorator = recoJetsCollection + "." + m_dPtKey;
  m_matchDecorator = recoJetsCollection + "." + m_matchKey;

  ATH_CHECK(m_recoJets.initialize());
  ATH_CHECK(m_truthTaus.initialize());
  ATH_CHECK(m_drDecorator.initialize());
  ATH_CHECK(m_dPtDecorator.initialize());
  ATH_CHECK(m_matchDecorator.initialize());
  if (!m_linkKey.empty()) {
    m_linkDecorator = recoJetsCollection + "." + m_linkKey;
    ATH_CHECK(m_linkDecorator.initialize());
  }

  // Configure jet selection logic based on properties
  using Part = xAOD::IParticle;
  float ptMin = m_minTruthTauPt.value();
  float drMax = m_maxDeltaR.value();

  // For truth taus, the algorithm checks if these are:
  // 10: isolated Tau particles, 11: non-isolated (from b-, c-meson decays)
  // https://gitlab.cern.ch/atlas/athena/-/blob/main/Generators/TruthUtils/TruthUtils/TruthClasses.h#L8
  // Only for isolated taus, the matching is done. Afterwards, build a TLorentzVector for the visible part of
  // the tau and do the matching.
  m_tauSelector = [drMax, ptMin](const Part* tj, const JV& sv) -> const Part* {
    const Part* bestMatch = nullptr;
    float minDeltaR = drMax;
    for (const auto* sj : sv) {
      if (acc_classifierParticleType(*sj) == 10) {
        TLorentzVector vec;
        vec.SetPtEtaPhiM(acc_pt_vis(*sj), acc_eta_vis(*sj), acc_phi_vis(*sj), acc_m_vis(*sj));

        float deltaR = tj->p4().DeltaR(vec);
        if (deltaR < minDeltaR && vec.Pt() > ptMin) {
            minDeltaR = deltaR;
            bestMatch = sj;
        }
      }
    }
    return bestMatch;
  };
  return StatusCode::SUCCESS;
}

namespace {
  using JC = xAOD::IParticleContainer;
  auto descending_pt = [](auto* j1, auto* j2){
    return j1->pt() > j2->pt();
  };
  std::vector<const xAOD::IParticle*> getJetVector(
    SG::ReadHandle<JC>& handle) {
    std::vector<const xAOD::IParticle*> jets(handle->begin(), handle->end());
    std::sort(jets.begin(),jets.end(), descending_pt);
    return jets;
  }
}

StatusCode TruthTauMatcherAlg::execute(const EventContext& cxt) const {
  SG::ReadHandle<JC> recoJetGet(m_recoJets, cxt);
  SG::WriteDecorHandle<JC,float> drDecorator(m_drDecorator, cxt);
  SG::WriteDecorHandle<JC,float> dPtDecorator(m_dPtDecorator, cxt);
  SG::WriteDecorHandle<JC,char> matchDecorator(m_matchDecorator, cxt);
  std::optional<SG::WriteDecorHandle<JC,IPLV>> linkDecorator;
  if (!m_linkDecorator.empty()) linkDecorator.emplace(m_linkDecorator, cxt);
  auto recoJets = getJetVector(recoJetGet);
  std::vector<const xAOD::IParticle*> truthTaus;
  std::map<const xAOD::IParticle*, const xAOD::IParticleContainer*> p2c;
  for (const auto& key: m_truthTaus) {
    const auto* cont = SG::ReadHandle<JC>(key, cxt).cptr();
    truthTaus.insert(truthTaus.end(), cont->begin(), cont->end());
    for (const auto* part: *cont) p2c[part] = cont;
  }
  std::sort(truthTaus.begin(), truthTaus.end(), descending_pt);
  std::vector<MatchedPair<JC>> matches;
  for (const xAOD::IParticle* recoJet: recoJets) {
    const xAOD::IParticle* truthTau = m_tauSelector(recoJet, truthTaus);
    if (truthTau) {
      TLorentzVector truthTauVis;
      truthTauVis.SetPtEtaPhiM(acc_pt_vis(*truthTau), acc_eta_vis(*truthTau), acc_phi_vis(*truthTau), acc_m_vis(*truthTau));
      drDecorator(*recoJet) = recoJet->p4().DeltaR(truthTauVis);
      dPtDecorator(*recoJet) = recoJet->pt() - acc_pt_vis(*truthTau);
      matchDecorator(*recoJet) = 1;
      matches.push_back({truthTau, recoJet});
      if (linkDecorator) linkDecorator.value()(*recoJet) = {
        {*p2c.at(truthTau), truthTau->index(), cxt}
      };
    } else {
      drDecorator(*recoJet) = NAN;
      dPtDecorator(*recoJet) = NAN;
      matchDecorator(*recoJet) = 0;
      matches.push_back({nullptr, recoJet});
      if (linkDecorator) linkDecorator.value()(*recoJet) = {};
    }
  }
  m_floats.copy(matches, cxt);
  m_ints.copy(matches, cxt);
  m_uints.copy(matches, cxt);
  m_chars.copy(matches, cxt);
  m_iparticles.copy(matches, cxt);

  return StatusCode::SUCCESS;
}
StatusCode TruthTauMatcherAlg::finalize () {
  return StatusCode::SUCCESS;
}