#include "TruthWriter.hh"
#include "HDF5Utils/Writer.h"

// Less Standard Libraries (for atlas)
#include "H5Cpp.h"

// ATLAS things
#include "xAODBTagging/BTaggingUtilities.h"
#include "xAODJet/Jet.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
template <typename T>
using Acc = SG::AuxElement::ConstAccessor<T>;

#include "TruthTools.hh" // longer parentBarcode method


namespace {
  Amg::Vector3D v3(const xAOD::TruthVertex& p) {
    return {p.x(), p.y(), p.z()};
  }
  Amg::Vector3D d3(const TruthOutputs& out) {
    if ( out.truth->decayVtx() ) {
      return v3(*out.truth->decayVtx()) - out.origin;
    }
    else return {NAN, NAN, NAN};
  }
  Amg::Vector3D p3(const xAOD::Jet& j) {
    return {j.px(), j.py(), j.pz()};
  }
}

typedef std::function<float(const TruthOutputs&)> FloatFiller;
typedef std::function<int(const TruthOutputs&)> IntFiller;
typedef std::function<char(const TruthOutputs&)> CharFiller;
typedef std::function<unsigned char(const TruthOutputs&)> UCharFiller;
typedef std::function<unsigned int(const TruthOutputs&)> UIntFiller;
typedef std::function<double(const TruthOutputs&)> DoubleFiller;


class TruthConsumers: public H5Utils::Consumers<const TruthOutputs&> {};
class TruthOutputWriter: public H5Utils::Writer<1,const TruthOutputs&>
{
public:
  TruthOutputWriter(H5::Group& file,
                    const std::string& name,
                    const TruthConsumers& cons,
                    size_t size):
    H5Utils::Writer<1,const TruthOutputs&>(file, name, cons, {{size}}) {}
};

TruthWriter::TruthWriter(
  H5::Group& output_file,
  const std::size_t output_size,
  const std::string& link_name,
  const std::string& output_name,
  TrackSortOrder order,
  const VariablesByType& vars):
  m_hdf5_truth_writer(nullptr),
  m_acc(link_name),
  m_output_size(output_size)
{
  using TO = TruthOutputs;
  const auto h = H5Utils::Compression::HALF_PRECISION;
  if ( order == TrackSortOrder::PT ) {
    m_sort = [](const xAOD::TruthParticle* p1, const xAOD::TruthParticle* p2) {
      return (p1->pt() > p2->pt());
    };
  } else {
    throw std::logic_error("undefined sort order");
  }

  TruthConsumers fillers;

    // hard coded fillers
  FloatFiller pt = [](const TO& t) -> float {
    return t.truth->pt();
  };
  FloatFiller mass = [](const TO& t) -> float {
    return t.truth->m();
  };
 FloatFiller e = [](const TO& t) -> float {
    return t.truth->e();
  };
  FloatFiller eta = [](const TO& t) -> float {
    return t.truth->eta();
  };
  FloatFiller phi = [](const TO& t) -> float {
    return t.truth->phi();
  };
  FloatFiller deta = [](const TO& t) -> float {
    return t.jet->eta() - t.truth->eta();
  };
  FloatFiller dphi = [](const TO& t) -> float {
    return t.jet->p4().DeltaPhi(t.truth->p4());
  };
  FloatFiller dr = [](const TO& t) -> float {
    return t.jet->p4().DeltaR(t.truth->p4());
  };
  FloatFiller Lxy = [](const TO& t) -> float {
    return d3(t).perp();
  };

  FloatFiller decayVertexX = [](const TO& t) -> float {
    return d3(t).x();
  };
  FloatFiller decayVertexY = [](const TO& t) -> float {
    return d3(t).y();
  };
  FloatFiller decayVertexZ = [](const TO& t) -> float {
    return d3(t).z();
  };

  FloatFiller decayVertexDR = [](const TO& t) {
    return d3(t).deltaR(p3(*t.jet));
  };
  FloatFiller decayVertexDPhi = [](const TO& t) {
    return p3(*t.jet).deltaPhi(d3(t));
  };
  FloatFiller decayVertexDEta = [](const TO& t) {
    return d3(t).eta() - p3(*t.jet).eta();
  };


  IntFiller charge = [](const TruthOutputs& t) -> int {
    return t.truth->charge();
  };
  IntFiller flavour = [](const TruthOutputs& t) -> int {
    if ( t.truth->isBottomHadron() ) { return 5; }
    if ( t.truth->isCharmHadron()  ) { return 4; }
    return -1;
  };
  IntFiller pdgId = [](const TruthOutputs& t) -> int {
    return t.truth->pdgId();
  };
  IntFiller barcode = [](const TruthOutputs& t) -> int {
    return t.truth->barcode();
  };

  for(const auto& var: vars.ints){
    if(var == "charge") fillers.add("charge", charge, -2);
    else if(var == "flavour") fillers.add("flavour", flavour, -1);
    else if(var == "pdgId") fillers.add("pdgId", pdgId, -1);
    else if(var == "barcode") fillers.add("barcode", barcode, -1);
    else{
      IntFiller ff = [acc=Acc<int>(var)](const TruthOutputs& t) -> int {
          return acc(*t.truth);
      };
      fillers.add(var, ff, -1);
    }
  }
  for(const auto& var: vars.floats){
    if(var == "pt") fillers.add("pt", pt, NAN);
    else if(var == "mass") fillers.add("mass", mass, NAN);
    else if(var == "energy") fillers.add("energy", e, NAN);
    else if(var == "eta") fillers.add("eta", eta, NAN, h);
    else if(var == "phi") fillers.add("phi", phi, NAN, h);
    else if(var == "deta") fillers.add("deta", deta, NAN, h);
    else if(var == "dphi") fillers.add("dphi", dphi, NAN, h);
    else if(var == "dr") fillers.add("dr", dr, NAN, h);
    else if(var == "Lxy") fillers.add("Lxy", Lxy, NAN, h);
    else if(var == "decayVertexX") fillers.add("decayVertexX", decayVertexX, NAN, h);
    else if(var == "decayVertexY") fillers.add("decayVertexY", decayVertexY, NAN, h);
    else if(var == "decayVertexZ") fillers.add("decayVertexZ", decayVertexZ, NAN, h);
    else if(var == "decayVertexDR") fillers.add("decayVertexDR", decayVertexDR, NAN, h);
    else if(var == "decayVertexDPhi") fillers.add("decayVertexDPhi", decayVertexDPhi, NAN, h);
    else if(var == "decayVertexDEta") fillers.add("decayVertexDEta", decayVertexDEta, NAN, h);
    else{
      FloatFiller ff = [acc=Acc<float>(var)](const TruthOutputs& t) -> float {
          return acc(*t.truth);
      };
      fillers.add(var, ff, NAN);
    }
  }

  for(const auto& var: vars.doubles){
    DoubleFiller df = [acc=Acc<double>(var)](const TruthOutputs& t) -> double {
        return acc(*t.truth);
    };
    fillers.add(var, df, NAN);
  }

  for(const auto& var: vars.ints_as_halves){
    FloatFiller hf = [acc=Acc<int>(var)](const TruthOutputs& t) -> float {
        return acc(*t.truth);
    };
    fillers.add(var, hf, -1, h);
  }

  for(const auto& var: vars.halves){
    FloatFiller hf = [acc=Acc<float>(var)](const TruthOutputs& t) -> float {
        return acc(*t.truth);
    };
    fillers.add(var, hf, -1, h);
  }

  for(const auto& var: vars.chars){
    CharFiller cf = [acc=Acc<char>(var)](const TruthOutputs& t) -> char {
        return acc(*t.truth);
    };
    fillers.add(var, cf, -1);
  }

  for(const auto& var: vars.uchars){
    UCharFiller ucf = [acc=Acc<unsigned char>(var)](const TruthOutputs& t) -> unsigned char {
        return acc(*t.truth);
    };
    fillers.add(var, ucf, 0);
  }

  for(const auto& var: vars.uints){
    UIntFiller uif = [acc=Acc<unsigned int>(var)](const TruthOutputs& t) -> unsigned int {
        return acc(*t.truth);
    };
    fillers.add(var, uif, 0);
  }

  // add valid flag, for more robust selection, true for any truth particle
  // that is defined.
  fillers.add("valid", [](const auto&) { return true; }, false);

  // build the output dataset
  if (link_name.size() == 0) {
    throw std::logic_error("output name not specified");
  }
  if (m_output_size == 0) {
    throw std::logic_error("can't make an output writer with no truth particles!");
  }
  m_hdf5_truth_writer.reset(
    new TruthOutputWriter(
      output_file, output_name, fillers, m_output_size));
}

TruthWriter::~TruthWriter() {
  if (m_hdf5_truth_writer) m_hdf5_truth_writer->flush();
}

TruthWriter::TruthWriter(TruthWriter&&) = default;

void TruthWriter::write(const xAOD::Jet& jet, const Amg::Vector3D& origin) {
  if (m_hdf5_truth_writer) {

    // get linked truth particles
    auto truth_particles = get_truth_parts(jet);

    // get information about each truth
    std::vector<TruthOutputs> truth_outputs;
    for (const auto* truth: truth_particles) {
      truth_outputs.push_back(
        TruthOutputs{
          truth,
          &jet,
          origin
        });
    }

    // write
    m_hdf5_truth_writer->fill(truth_outputs);
  }
}
void TruthWriter::write_dummy() {
  if (m_hdf5_truth_writer) {
    std::vector<TruthOutputs> truth_outputs;
    m_hdf5_truth_writer->fill(truth_outputs);
  }
}

// access decorated particles
TruthWriter::Truths TruthWriter::get_truth_parts(const xAOD::Jet& jet) const
{
  Truths truth_particles;
  for (const auto& link: m_acc(jet)) {
    if (!link.isValid()) {
      throw std::logic_error("invalid truth link");
    }
    const xAOD::IParticle* part = *link;
    const auto* truth = dynamic_cast<const xAOD::TruthParticle*>(part);
    if (!truth) {
      throw std::runtime_error(
        "Truth writer could not cast xAOD::IParticle to xAOD::TruthParticle");
    }
    truth_particles.push_back(truth);
  }
  std::sort(truth_particles.begin(), truth_particles.end(), m_sort);
  return truth_particles;
}
