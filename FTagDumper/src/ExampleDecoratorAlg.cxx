#include "ExampleDecoratorAlg.h"
#include "StoreGate/WriteDecorHandle.h"

ExampleDecoratorAlg::ExampleDecoratorAlg(const std::string& name,
                       ISvcLocator* loc):
  AthReentrantAlgorithm(name, loc)
{
}

StatusCode ExampleDecoratorAlg::initialize() {
  if (m_exampleKey.key().empty()) {
    // ToDo: figure out how to get the name of the key from the key
    ATH_MSG_ERROR("you must specify a name for 'decoratorValue'");
  }
  ATH_CHECK(m_exampleKey.initialize());
  return StatusCode::SUCCESS;
}
StatusCode ExampleDecoratorAlg::execute(const EventContext& cxt) const {
  SG::WriteDecorHandle<xAOD::IParticleContainer, float> example(
    m_exampleKey, cxt);
  for (const xAOD::IParticle* jet: *example) {
    example(*jet) = m_decoratorValue;
  }
  return StatusCode::SUCCESS;
}
StatusCode ExampleDecoratorAlg::finalize() {
  return StatusCode::SUCCESS;
}
