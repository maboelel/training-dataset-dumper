#
# Basic setup for dumping Tagging information
#

# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.14 FATAL_ERROR )

# Setup this project
project( Dumpster VERSION 1.0 LANGUAGES CXX C )

# Bail out if no release is setup
if ( NOT DEFINED ENV{AtlasProject} )
  message(FATAL_ERROR "AtlasProject environment variable not set, can't figure out release")
endif()

# Figure out what release we're in (e.g. x.y.z)
set ( AtlasVersion $ENV{$ENV{AtlasProject}_VERSION} )

# Set up that release
find_package( $ENV{AtlasProject} ${AtlasVersion} REQUIRED )

# Set up CTest:
atlas_ctest_setup()

# ATLAS wants to deprecate calls to AuxElement::auxdata, but we are
# dragging our feet and at the moment don't even print a warning. This
# turns on the deprecation warning. But...
add_compile_definitions(XAOD_DEPRECATE_AUXDATA=1)

# We don't want any warnings in compilation. This promotes them to
# errors.
add_compile_options(-Werror)

# Set up a work directory project:
atlas_project( Dumpster 1.0 USE $ENV{AtlasProject} ${AtlasVersion} )

# Set up the runtime environment setup script(s):
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
message(STATUS "Appending Dumpster tab complete to setup")
file(APPEND ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh.in
  "# custom Dumpster scripts:\n"
  "TC=\${Dumpster_DIR}/tab-complete.bash\n"
  "[ -f $TC ] && . \${Dumpster_DIR}/tab-complete.bash\n"
  "unset TC\n"
  )

install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Set up CPack:
atlas_cpack_setup()
