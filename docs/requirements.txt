mkdocs >= 1.4.0
mkdocs-material >= 9.2.1
mkdocs-markdownextradata-plugin >= 0.2.5
mkdocs-mermaid2-plugin >= 0.5.2
mkdocs-git-revision-date-localized-plugin
mkdocs-autorefs==0.5.0
mkdocstrings[python]
